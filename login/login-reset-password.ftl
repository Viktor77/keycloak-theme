<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('username','password') displayInfo=realm.password && realm.registrationAllowed && !registrationDisabled??; section>
    <#--  <#if section = "header">
        ${msg("Login to you account")}  -->
    <#if section = "header">
      ${msg("")}
    <#elseif section = "form">
    <div id="select-container">
    <button id="goBackButton" style="border:2px solid #51A1DA; color:#51A1DA; border-radius:4px; padding:5px 15px;">go back</button>
    <select id="select" class="select" onChange="handleLanguageChange()">
        <option value="en">en</option>
        <option value="ka">ka</option>
        <option value="ru">ru</option>
    </select>
    </div>

    <div id="kc-form">
      <div id="kc-form-wrapper">
        <#if realm.password>
        <div class="images-wrapper">
        <svg class="register-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 500">
			<svg xmlns="http://www.w3.org/2000/svg" id="kc-login-doctor-svg" viewBox="-10 35 550 550">
		      <path fill-rule="evenodd" clip-rule="evenodd" fill="transparent" stroke-width="3" stroke="#f59033" d="M344 288 L38 217L174 42L390 227L344 288Z"/>
		      <text class="text-svg" x="200" y="190" dominant-baseline="middle" text-anchor="middle" fill="#f59033" id="doctor">
                specialist
		       </text>
	    	</svg>
        	<svg xmlns="http://www.w3.org/2000/svg" id="kc-login-patient-svg" viewBox="55 -270 600 600">
		      <path fill-rule="evenodd" clip-rule="evenodd" fill="transparent" stroke-width="3" stroke="#0082C8" d="M0 0 L96 127 L305 74 L239 13 L0 0Z"/>
			  <text class="text-svg" x="150" y="55" dominant-baseline="middle" text-anchor="middle" fill="#0082C8" id="patient" >
			   patient
			  </text>
	 	    </svg>
            <#--  <svg xmlns="http://www.w3.org/2000/svg" id="kc-login-organization-svg" viewBox="-220 -65 700 250" >
			  <path fill-rule="evenodd" clip-rule="evenodd" fill="transparent" stroke-width="3" stroke="#588378" d="M342 232L6 160L274 80L464 197L342 232"/>
			  <text class="text-svg" x="250" y="160" dominant-baseline="middle" text-anchor="middle" fill="#588378" id="organization">
				organization
			  </text>
		    </svg>  -->
            <#--  <svg xmlns="http://www.w3.org/2000/svg" id="kc-login-user-svg" viewBox="-70 -450 610 620">
			  <path fill-rule="evenodd" clip-rule="evenodd" fill="transparent" stroke-width="3" stroke="#679cac" d="M44 0 L315 64 L262 149 L0 165Z" />
			  <text class="text-svg" x="155" y="95" dominant-baseline="middle" text-anchor="middle" fill="#679cac" id="user">
				user
		      </text>
		    </svg>  -->
		</svg>
        <div class="info-link-wrapper">
          <a class="info-link" href="#"><img src="${url.resourcesPath}/img/instagram.svg"/></a>
          <a class="info-link" href="#"><img src="${url.resourcesPath}/img/twitter.svg"/></a>
          <a class="info-link" href="#"><img src="${url.resourcesPath}/img/fb.svg"/></a>
        </div>
        </div>
            <form  id="kc-reset-password-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <h1 class="headline-main" id="forgotPassword">Forgot password?</h1>

                <div id="kc-username-field">
                    <label id="kc-username-label" for="username" class="${properties.kcLabelClass!}"><#if !realm.loginWithEmailAllowed><span id="email">email</span><#elseif !realm.registrationEmailAsUsername><span id="email">email</span><#else><span id="email">email</span></#if></label>

                    <#if usernameEditDisabled??>
                        <input tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}" type="text" disabled />
                    <#else>
                        <input tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}"  type="text" autofocus autocomplete="off"
                               aria-invalid="<#if messagesPerField.existsError('username','password')>true</#if>"
                        />

                        <#if messagesPerField.existsError('username')>
                            <span id="input-error" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                    ${kcSanitize(messagesPerField.getFirstError('username'))?no_esc}
                            </span>
                        </#if>
                    </#if>
                </div>

                <div class="form-backtologin ${properties.kcFormSettingClass!}">
                        <div class="${properties.kcFormOptionsWrapperClass!}">
                                <span><a tabindex="5" href="${url.loginUrl}" id="goToLogin">${msg("goToLogin")}</a></span>
                        </div>
                  </div>

                  <div id="kc-form-buttons">
                      <input type="hidden" id="id-hidden-input" name="credentialId" <#if auth.selectedCredential?has_content>value="${auth.selectedCredential}"</#if>/>
                      <input tabindex="4" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" name="login" id="kc-forgot-password" type="submit" value="${msg("resetPassword")}" />
                  </div>
            </form>
        </#if>
        </div>

        <#if realm.password && social.providers??>
            <div id="kc-social-providers" class="${properties.kcFormSocialAccountSectionClass!}">
                <hr/>
                <h4>${msg("identity-provider-login-label")}</h4>

                <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountListGridClass!}</#if>">
                    <#list social.providers as p>
                        <a id="social-${p.alias}" class="${properties.kcFormSocialAccountListButtonClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                type="button" href="${p.loginUrl}">
                            <#if p.iconClasses?has_content>
                                <i class="${properties.kcCommonLogoIdP!} ${p.iconClasses!}" aria-hidden="true"></i>
                                <span class="${properties.kcFormSocialAccountNameClass!} kc-social-icon-text">${p.displayName!}</span>
                            <#else>
                                <span class="${properties.kcFormSocialAccountNameClass!}">${p.displayName!}</span>
                            </#if>
                        </a>
                    </#list>
                </ul>
            </div>
        </#if>

    </div>
    <script>
        const select = document.getElementById("select");
        select.classList.add("select-none");
        const resetBtn = document.getElementById('kc-forgot-password');
        resetBtn.addEventListener("click", () => { handleLanguageChange() });
        document.getElementById("goBackButton").onclick = function () {
        window.history.back()
        };
        const language = {
                en: {
                    doctor:"specialist",
                    patient:"patient",
                    organization:"organization",
                    user:"user",
                    username:"username",
                    usernameOrEmail:"username or email",
                    email:"Email",
                    logToYourAccount:"Log to your account",
                    dontHaveAccount:"Don't have an account?",
                    createAccount:"Create account!",
                    passwordLabel:"password",
                    rememberMeLabel:"Remember me",
                    loginNow:"Login now",
                    "input-error": "Invalid email.",
                    forgotPassword:"Forgot Password?",
                    goBackButton:"back",
                    goToLogin:"Back to login",
                    resetPassword:"Reset password"
                },
                ka: {
                    doctor:"სპეციალისტი",
                    patient:"პაციენტი",
                    organization:"ორგანიზაცია",
                    user:"მომხამარებელი",
                    username:"მომხმარებელი",
                    usernameOrEmail:"მომხმარებელი ან ელ.ფოსტა",
                    email:"ელ.ფოსტა",
                    logToYourAccount:"ანგარიშის აქტივაცია",
                    dontHaveAccount:"არ მაქვს ანგარიში",
                    createAccount:"ანგარიშის შექმნა",
                    passwordLabel:"პაროლი",
                    rememberMeLabel:"დამახსოვრება",
                    loginNow:"სისტემაში შესვლა",
                    "input-error": "Invalid email",
                    forgotPassword:"დაგავიწყდათ პაროლი?",
                    goBackButton:"უკან",
                     goToLogin:"უკან შესვლაზე",
                     resetPassword:"შეცვალე პასვორდი"
                },
                ru: {
                    doctor:"Специалист",
                    patient:"пациент",
                    organization:"Организация",
                    user:"Пользователь",
                    username:"Имя пользователя",
                    usernameOrEmail:"Имя пользователя или e-mail",
                    email:"E-mail",
                    logToYourAccount:"Войти в аккаунт",
                    dontHaveAccount:"Нет аккаунта?",
                    createAccount:"Создать аккаунт!",
                    passwordLabel:"Пароль",
                    rememberMeLabel:"Запомнить",
                    loginNow:"Войти в аккаунт",
                    "input-error":"Не верный имейл",
                    forgotPassword:"Забыли пароль?",
                    goBackButton:"назад",
                     goToLogin:"На страницу входа",
                     resetPassword:"Восстановить пароль"
                }
    }

    let locale = localStorage.getItem("localeId") ? localStorage.getItem("localeId")==="en-us" ? "en":localStorage.getItem("localeId"):"en"

    if(locale) {
        document.getElementById("select").value = locale;
        handleLanguageChange();
    }
    function handleLanguageChange () {
        const selectedLanguage = document.getElementById("select").value;
        localStorage.setItem("localeId",selectedLanguage)
        Object.keys(language.en).map((text)=>{
            const textSpan = document.getElementById(text)
            if (textSpan) {
                textSpan.textContent = language[selectedLanguage][text]
            }
        })
        resetBtn.value = language[selectedLanguage].resetPassword;
    }

    </script>
    <#elseif section = "info" >
    </#if>

</@layout.registrationLayout>


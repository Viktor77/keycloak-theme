<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=!messagesPerField.existsError('firstName','lastName','email','username','password','password-confirm'); section>
    <#--  <#if section = "header">
        ${msg("Create your account")}  -->
    <#if section = "header">
        ${msg("")}
    <#elseif section = "form">
       <div class="select-container">   
           <button id="goBackButton" style="border:2px solid #51A1DA; color:#51A1DA; border-radius:4px; padding:5px 15px;">go back</button>
        <select id="languageSelect" onChange="handleLanguageChange()" class="select">
            <option value="en">en</option>
            <option value="ka">ka</option>
            <option value="ru">ru</option>
   </select>
    </div>

    <div class="register-wrapper">
        <div class="images-wrapper">
        <h2 class="headline-sec" id="joinPlatformAs">JOIN YOUR HEALTH PLATFORM AS</h2>
        <svg class="register-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 500">
			<svg xmlns="http://www.w3.org/2000/svg" id="kc-register-doctor-svg" viewBox="-10 35 550 550">
		      <path fill-rule="evenodd" clip-rule="evenodd" fill="#f59033" stroke-width="3" stroke="#f59033" d="M344 288 L38 217L174 42L390 227L344 288Z"/>
		      <text class="text-svg" x="200" y="190" dominant-baseline="middle" text-anchor="middle" fill="#FFF" id="doctor">
			   specialist
		       </text>
	    	</svg>
        	<svg xmlns="http://www.w3.org/2000/svg" id="kc-register-patient-svg" viewBox="55 -270 600 600">
		      <path fill-rule="evenodd" clip-rule="evenodd" fill="#0082C8" stroke-width="3" stroke="#0082C8" d="M0 0 L96 127 L305 74 L239 13 L0 0Z"/>
			  <text class="text-svg" x="150" y="55" dominant-baseline="middle" text-anchor="middle" fill="#FFF" id="patient">
			    patient
			  </text>
	 	    </svg>
            <#--  <svg xmlns="http://www.w3.org/2000/svg" id="kc-register-organization-svg" viewBox="-220 -65 700 250" >
			  <path fill-rule="evenodd" clip-rule="evenodd" fill="#588378" stroke-width="3" stroke="#588378" d="M342 232L6 160L274 80L464 197L342 232"/>
			  <text class="text-svg" x="250" y="160" dominant-baseline="middle" text-anchor="middle" fill="#FFF" id="organization">
				organization
			  </text>
		    </svg>  -->
              <#--  <svg xmlns="http://www.w3.org/2000/svg" id="kc-register-user-svg" viewBox="-70 -450 610 620">
			  <path fill-rule="evenodd" clip-rule="evenodd" fill="#679cac" stroke-width="3" stroke="#679cac" d="M44 0 L315 64 L262 149 L0 165Z" />
			  <text class="text-svg" x="155" y="95" dominant-baseline="middle" text-anchor="middle" fill="#FFF" id="user">
				user
		      </text>
		    </svg>   -->
		</svg>
        <div class="info-link-wrapper">
          <a class="info-link" href="#"><img src="${url.resourcesPath}/img/instagram.svg"/></a>
          <a class="info-link" href="#"><img src="${url.resourcesPath}/img/twitter.svg"/></a>
          <a class="info-link" href="#"><img src="${url.resourcesPath}/img/fb.svg"/></a>
        </div>
        </div>
        <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post">
        <h1 id="kc-register-main-headline" class="headline-main ${properties.kcFormGroupClass!}">Create your account</h1>
        <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                        <div style="margin:20px 0"><span id="alreadyHaveAccount" style="font-size:16px;">Already have an account?</span><a id="login-form-anchor" class="" href="${url.loginUrl}"><span id="signInNow"  style="font-size:16px;">Sign In Now</span></a></div>
                    </div>
                </div>
            <div id="kc-register-first-name-field" class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}" ><span id="firstNameLabel">First name</span></label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" class="${properties.kcInputClass!}" name="firstName"
                           value="${(register.formData.firstName!'')}"
                           aria-invalid="<#if messagesPerField.existsError('firstName')>true</#if>"
                    />

                    <#if messagesPerField.existsError('firstName')>
                        <span id="input-error-firstname" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('firstName'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div id="kc-register-last-name-field" class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}"><span id="lastNameLabel">Last name</span></label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" class="${properties.kcInputClass!}" name="lastName"
                           value="${(register.formData.lastName!'')}"
                           aria-invalid="<#if messagesPerField.existsError('lastName')>true</#if>"
                    />

                    <#if messagesPerField.existsError('lastName')>
                        <span id="input-error-lastname" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('lastName'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div id="kc-register-organization-field" class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.organization" class="${properties.kcLabelClass!}">Organization name</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input id="kc-register-organization-input" type="text" id="user.attributes.organization" class="${properties.kcInputClass!}" name="user.attributes.organization"
                           value="${(register.formData['user.attributes.organization']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('organization')>true</#if>"
                    />

                    <#if messagesPerField.existsError('organization')>
                        <span id="input-error-user-attribute-organization" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('organization'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <#if !realm.registrationEmailAsUsername>
                <div id="kc-register-username-field" class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label id="kc-register-username-label" for="username" class="${properties.kcLabelClass!}"><span id="usernameLabel">Username</span></label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="text" id="username" class="${properties.kcInputClass!}" name="username"
                               value="${(register.formData.username!'')}" autocomplete="username"
                               aria-invalid="<#if messagesPerField.existsError('username')>true</#if>"
                        />

                        <#if messagesPerField.existsError('username')>
                            <span id="input-error-username" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('username'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>
            </#if>

            <div id="kc-register-phone-number-field" class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.phonenumber" class="${properties.kcLabelClass!}"><span id="phoneNumberLabel">Phone number</span></label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="user.attributes.phonenumber" class="${properties.kcInputClass!}" name="user.attributes.phonenumber"
                           value="${(register.formData['user.attributes.phonenumber']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('phonenumber')>true</#if>"
                    />

                    <#if messagesPerField.existsError('phonenumber')>
                        <span id="input-error-user-attribute-phonenumber" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('phonenumber'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}"><span id="emailLabel">Email</span></label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="email" class="${properties.kcInputClass!}" name="email"
                           value="${(register.formData.email!'')}" autocomplete="email"
                           aria-invalid="<#if messagesPerField.existsError('email')>true</#if>"
                    />

                    <#if messagesPerField.existsError('email')>
                        <span id="input-error-email" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('email'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div id="kc-register-role-field" class="${properties.kcFormGroupClass!} hidden-field">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.role" class="${properties.kcLabelClass!}">Role</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="user.attributes.role" class="${properties.kcInputClass!}" name="user.attributes.role"
                           value="${(register.formData['user.attributes.role']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('party')>true</#if>"
                    />

                    <#if messagesPerField.existsError('role')>
                        <span id="input-error-user-attribute-role" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('role'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <#if passwordRequired??>
                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="password" class="${properties.kcLabelClass!}"><span id="passwordLabel">Password</span></label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="password" id="password" class="${properties.kcInputClass!}" name="password"
                               autocomplete="new-password"
                               aria-invalid="<#if messagesPerField.existsError('password','password-confirm')>true</#if>"
                        />

                        <#if messagesPerField.existsError('password')>
                            <span id="input-error-password" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>

                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="password-confirm"
                               class="${properties.kcLabelClass!}"><span id="passwordConfirmLabel">Confirm password</span></label>
                    </div>
                    <div class="${properties.kcInputWrapperClass!}">
                        <input type="password" id="password-confirm" class="${properties.kcInputClass!}"
                               name="password-confirm"
                               aria-invalid="<#if messagesPerField.existsError('password-confirm')>true</#if>"
                        />

                        <#if messagesPerField.existsError('password-confirm')>
                            <span id="input-error-password-confirm" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
                            </span>
                        </#if>
                    </div>
                </div>
            </#if>

            <div id="kc-register-terms-conditions-field" class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcInputWrapperClass!} d-flex">
                <input type="checkbox" id="kc-register-terms-conditions-input" name="terms-conditions" />
                    <p class="terms-text"><a id="termsAndConditionsAnchor">Terms & Conditions</a></p>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} hidden-field">
                <div class="${properties.kcLabelWrapperClass!}">
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="user.attributes.termsconditions" class="${properties.kcInputClass!}" name="user.attributes.termsconditions"
                           value="${(register.formData['user.attributes.termsconditions']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('termsconditions')>true</#if>"
                    />

                    <#if messagesPerField.existsError('termsconditions')>
                        <span id="input-error-user-attribute-termsconditions" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('termsconditions'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div id="kc-register-invite-id-field" class="${properties.kcFormGroupClass!} hidden-field">
                <div class="${properties.kcLabelWrapperClass!}">
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="user.attributes.inviteid" class="${properties.kcInputClass!}" name="user.attributes.inviteid"
                           value="${(register.formData['user.attributes.inviteid']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('inviteid')>true</#if>"
                    />

                    <#if messagesPerField.existsError('inviteid')>
                        <span id="input-error-user-attribute-inviteid" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('inviteid'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>
            <div id="kc-register-locale-field" class="${properties.kcFormGroupClass!} hidden-field">
                            <div class="${properties.kcLabelWrapperClass!}">
                            </div>
                            <div class="${properties.kcInputWrapperClass!}">
                                <input type="text" id="user.attributes.locale" class="${properties.kcInputClass!}" name="user.attributes.locale"
                                       value="${(register.formData['user.attributes.locale']!'')}"
                                       aria-invalid="<#if messagesPerField.existsError('locale')>true</#if>"
                                />

                                <#if messagesPerField.existsError('locale')>
                                    <span id="input-error-user-attribute-locale" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                                        ${kcSanitize(messagesPerField.get('locale'))?no_esc}
                                    </span>
                                </#if>
                            </div>
                        </div>


            <div id="kc-register-referral-id-field" class="${properties.kcFormGroupClass!} hidden-field">
                <div class="${properties.kcLabelWrapperClass!}">
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="user.attributes.referralid" class="${properties.kcInputClass!}" name="user.attributes.referralid"
                           value="${(register.formData['user.attributes.referralid']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('referralid')>true</#if>"
                    />

                    <#if messagesPerField.existsError('referralid')>
                        <span id="input-error-user-attribute-referralid" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('referralid'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <div id="kc-register-referral-type-field" class="${properties.kcFormGroupClass!} hidden-field">
                <div class="${properties.kcLabelWrapperClass!}">
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="user.attributes.referraltype" class="${properties.kcInputClass!}" name="user.attributes.referraltype"
                           value="${(register.formData['user.attributes.referraltype']!'')}"
                           aria-invalid="<#if messagesPerField.existsError('referraltype')>true</#if>"
                    />

                    <#if messagesPerField.existsError('referraltype')>
                        <span id="input-error-user-attribute-referraltype" class="${properties.kcInputErrorMessageClass!}" aria-live="polite">
                            ${kcSanitize(messagesPerField.get('referraltype'))?no_esc}
                        </span>
                    </#if>
                </div>
            </div>

            <#if recaptchaRequired??>
                <div class="form-group">
                    <div class="${properties.kcInputWrapperClass!}">
                        <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                    </div>
                </div>
            </#if>

            <div id="modal" style="display: none;" class="modal-container">
                <div class="modal-content">
                <div id="termsAndConditionsText" style="white-space:pre-wrap;"></div>
            <div class="buttons-container">
                <input class="decline-btn" type="button" id="declineBtn" value="Decline" />
                <input id="agreeTermsAndConditions" class="accept-btn" type="button" value="Accept" />
            </div>
            </div>
            </div>
            <div id="kc-form-buttons" style="padding-left:0;" class="${properties.kcFormButtonsClass!}">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!}" type="submit" value="Create an account" id="createAccountBtn" disabled="true" style="opacity:0.5;" />
                </div>
            </div>

            </div>
        </form>
    </div>
     <script>
     const termsAndConditionsAnchor = document.getElementById("termsAndConditionsAnchor");
     const agreeToTermsAndConditionsButton = document.getElementById("agreeTermsAndConditions");
     const declineTermsAndConditionsButton = document.getElementById("declineBtn");
     const modal = document.getElementById("modal");
     const agreeToTermsCheckbox = document.getElementById("kc-register-terms-conditions-input");
     const termsAndConditionState = document.getElementById("user.attributes.termsconditions")
     const createAccountButton = document.getElementById("createAccountBtn");
     const contentWrapper = document.getElementById("kc-content-wrapper");
     const termsAndConditionsText = document.getElementById("termsAndConditionsText");
     createAccountButton.addEventListener("click", () => { handleLanguageChange() });

    contentWrapper.style.position = 'relative';

       document.getElementById("goBackButton").onclick = function () {
    window.history.back()
            };

    termsAndConditionsAnchor.onclick = function () {
         modal.style.display = "block";
     }

    declineTermsAndConditionsButton.onclick = function () {
         modal.style.display = "none";
         agreeToTermsCheckbox.checked = false;
         termsAndConditionState.value = "denied";
    }

    agreeToTermsAndConditionsButton.onclick = function () {
        createAccountButton.disabled = false;
        createAccountButton.style.opacity = "1";
        agreeToTermsCheckbox.checked = true;
        termsAndConditionState.value = "approved";
        modal.style.display = "none";
    }

    agreeToTermsCheckbox.addEventListener('change', (event) => {
    if (event.currentTarget.checked) {
        modal.style.display = "block";
        agreeToTermsCheckbox.checked = false;
    } else {
        termsAndConditionState.value = "denied";
        createAccountBtn.disabled = true;
        createAccountButton.style.opacity = "0.5";
    }
});
    const role = localStorage.getItem("role")

    const createAccountStrings = {
    en: {
      PATIENT_HEADLINE: "Create patient account",
      DOCTOR_HEADLINE: "Create doctor account",
      ORGANIZATION_HEADLINE: "Create organization account",
      USER_HEADLINE: "Create your account",
    },
    ka: {
      PATIENT_HEADLINE: "პაციენტის ანგარიშის შექმნა",
      DOCTOR_HEADLINE: "ექიმის ანგარიშის შექმნა",
      ORGANIZATION_HEADLINE: "ორგანიზაციის ანგარიშის შექმნა",
      USER_HEADLINE: "ანგარიშის შექმნა",
    },
    ru: {
      PATIENT_HEADLINE: "Создать аккаунт пациента",
      DOCTOR_HEADLINE: "Создать аккаунт врача",
      ORGANIZATION_HEADLINE: "Создать аккаунт организации",
      USER_HEADLINE: "Создать аккаунт",
    },
  };
  


let cachedData = null;
const termsAndConditions = () => {
    if (cachedData) {
            displayTerms(cachedData);
    } else {
            fetch('https://int.mrecords.me/product/v1/termsandconditions?page=0&size=20')
            .then(response => response.json())
            .then((data) => {
            cachedData = data;
            displayTerms(data);
    })
    .catch(err => {
            document.getElementById("termsAndConditionsText").innerHTML = "Could not get terms and conditions";
            document.getElementById("agreeToTermsAndConditionsButton").style.display = 'none';
    });
};
};

 const displayTerms = (data) => {
        const selectedLanguage = document.getElementById("languageSelect").value;
        let code;
        switch (selectedLanguage) {
            case 'ru':
            code = '1049';
            break;
            case 'ka':
            code = '1079';
            break;
            default:
            code = '1033';
        }
        document.getElementById("termsAndConditionsText").innerHTML = data.content.find((el) => el.entity.locale === code).text;
};


    const language = {
        en: {
         goBackButton:"back",
            joinPlatformAs:"JOIN YOUR  HEALTH PLATFORM AS",
            doctor:"specialist",
            patient:"patient",
            organization:"organization",
            user:"user",
            usernameLabel:"username",
            usernameOrEmail:"username or email",
            firstNameLabel:"First name",
            lastNameLabel:"Last name",
            phoneNumberLabel:"Phone number",
            emailLabel:"Email",
            passwordLabel:"password",
            passwordConfirmLabel:"Confirm password",
            termsAndConditionsLabel:"Terms & Conditions",
            termsAndConditionsAnchor:"Terms & Conditions",
            iAgreeWithTermsAndConditions:"I agree with terms & conditions",
            alreadyHaveAccount:"Already have an account?",
            signInNow:"Sign In Now",
            createAccountBtn:"Create an account",
            decline:"Decline",
            accept:"Accept",
            "kc-register-main-headline":"Create account",
            goBackLink:"back",
            "Username already exists.":"Username already exists.",
            "Email already exists.":"Email already exists.",
            "Password confirmation doesn't match.":"Password confirmation doesn't match.",
            "Invalid email address.":"Invalid email address. Example: example@anyemail.com",
            "Pease specify first name.":"Pease specify first name.",
            "Please specify last name.":"Please specify last name.",
            "Please specify username.":"Please specify username.",
            "Please specify email.":"Please specify email.",
            "Please specify password.":"Please specify password.",
            "Invalid email address.":"Invalid email address. Example: example@anyemail.com",

            "Введите имя":"Pease specify first name.",
            "Введите фамилию":"Please specify last name.",
            "Введите имя пользователя":"Please specify username.",
            "Введите электронный адрес":"Please specify email.",
            "Введите пароль":"Please specify password.",
            "Имя пользователя уже существует.":"Username already exists.",
            "E-mail уже существует.":"Email already exists.",
            "Пароли не совпадают":"Password confirmation doesn't match.",
            "Неверный формат электронного адреса. Пример:example@anyemail.com":"Invalid email address. Example: example@anyemail.com",

"გთხოვთ მიუთითეთ სახელი":"Pease specify first name.",
"გთხოვთ მიუთითეთ გვარი":"Please specify last name.",
"გთხოვთ მიუთითეთ მომხმარებელი":"Please specify username.",
"გთხოვთ მიუთითეთ ელ.მისამართი":"Please specify email.",
"გთხოვთ მიუთითეთ პაროლი":"Please specify password.",
"ასეთი მომხმარებული უკვე რეგისტრირებულია":"Username already exists.",
"ასეთი ელ. ფოსტა უკვე რეგისტრირებულია":"Email already exists.",
"პაროლები არ ემთხვევა ერთმანეთს":"Password confirmation doesn't match.",
"ელ.ფოსტის მისამართი არასწორია. მაგ: example@anyemail.com":"Invalid email address. Example: example@anyemail.com",
        },
        ka: {
        goBackButton:"უკან",
            joinPlatformAs:"შეუერთდით თქვენი ჯანმრთელობის პლატფორმას",
            doctor:"სპეციალისტი",
            patient:"პაციენტი",
            organization:"ორგანიზაცია",
            user:"მომხმარებელი",
            usernameLabel:"მომხმარებლის სახელი",
            usernameOrEmail:"მომხმარებლის სახელი ან ელ.ფოსტა",
            firstNameLabel:"სახელი",
            lastNameLabel:"გვარი",
            phoneNumberLabel:"ტელეფონი",
            emailLabel:"ელ.ფოსტა",
            passwordLabel:"პაროლი",
            passwordConfirmLabel:"პაროლის დადასტურება",
            termsAndConditionsLabel:"ვადები და პირობები",
            termsAndConditionsAnchor:"ვადები და პირობები",
            iAgreeWithTermsAndConditions:"ვეთანხმები ვადებს და პირობებს",
            alreadyHaveAccount:"უკვე მაქვს ანგარიში",
            signInNow:"შესვლა",
            createAccountBtn:"ანგარიშის შექმნა",
            decline:"უარყოფა",
            accept:"თანხმობა",
            "kc-register-main-headline":"ანგარიშის შექმნა",
            "Pease specify first name.":"გთხოვთ მიუთითეთ სახელი",
            "Please specify last name.":"გთხოვთ მიუთითეთ გვარი",
            "Please specify username.":"გთხოვთ მიუთითეთ მომხმარებელი",
            "Please specify email.":"გთხოვთ მიუთითეთ ელ.მისამართი",
            "Please specify password.":"გთხოვთ მიუთითეთ პაროლი",
            "Username already exists.":"ასეთი მომხმარებული უკვე რეგისტრირებულია",
            "Email already exists.":"ასეთი ელ. ფოსტა უკვე რეგისტრირებულია",
            "Password confirmation doesn't match.":"პაროლები არ ემთხვევა ერთმანეთს",
            "Invalid email address.":"ელ.ფოსტის მისამართი არასწორია. მაგ: example@anyemail.com",
            "Invalid email address. Example: example@anyemail.com":"ელ.ფოსტის მისამართი არასწორია. მაგ: example@anyemail.com",

            "Введите имя":"გთხოვთ მიუთითეთ სახელი",
            "Введите фамилию":"გთხოვთ მიუთითეთ გვარი",
            "Введите имя пользователя":"გთხოვთ მიუთითეთ მომხმარებელი",
            "Введите электронный адрес":"გთხოვთ მიუთითეთ ელ.მისამართი",
            "Введите пароль":"გთხოვთ მიუთითეთ პაროლი",
            "Имя пользователя уже существует.":"ასეთი მომხმარებული უკვე რეგისტრირებულია",
            "E-mail уже существует.":"ასეთი ელ. ფოსტა უკვე რეგისტრირებულია",
            "Пароли не совпадают":"პაროლები არ ემთხვევა ერთმანეთს",
            "Неверный формат электронного адреса. Пример:example@anyemail.com":"ელ.ფოსტის მისამართი არასწორია. მაგ: example@anyemail.com",



            goBackLink:"უკან"
        },
        ru: {
        goBackButton:"назад",
            joinPlatformAs:"ПРИСОЕДИНЯЙТЕСЬ К ВАШЕЙ ЛИЧНОЙ ПЛАТФОРМЕ ЗДОРОВЬЯ",
            doctor:"Специалист",
            patient:"пациент",
            organization:"Организация",
            user:"Пользователь",
            usernameLabel:"Имя пользователя",
            usernameOrEmail:"Имя пользователя или e-mail",
            firstNameLabel:"Имя",
            lastNameLabel:"Фамилия",
            phoneNumberLabel:"Телефон",
            emailLabel:"E-mail",
            passwordLabel:"Пароль",
            passwordConfirmLabel:"Подтвердите пароль",
            termsAndConditionsLabel:"Сроки и условия",
            termsAndConditionsAnchor:"Сроки и условия",
            iAgreeWithTermsAndConditions:"Подтверждаю согласие со сроками и условиями",
            alreadyHaveAccount:"Уже есть действующий аккаунт?",
            signInNow:"Войти",
            createAccountBtn:"Создать аккаунт",
            decline:"Отклонить",
            accept:"Принять",
            "kc-register-main-headline":"Создать аккаунт",
            "Pease specify first name.":"Введите имя",
            "Please specify last name.":"Введите фамилию",
            "Please specify username.":"Введите имя пользователя",
            "Please specify email.":"Введите электронный адрес",
            "Please specify password.":"Введите пароль",
            "Username already exists.":"Имя пользователя уже существует.",
            "Email already exists.":"E-mail уже существует.",
            "Password confirmation doesn't match.":"Пароли не совпадают",
            "Invalid email address.":"Неверный формат электронного адреса. Пример:example@anyemail.com",
            "Invalid email address. Example: example@anyemail.com":"Неверный формат электронного адреса. Пример:example@anyemail.com",

"გთხოვთ მიუთითეთ სახელი":"Введите имя",
"გთხოვთ მიუთითეთ გვარი":"Введите фамилию",
"გთხოვთ მიუთითეთ მომხმარებელი":"Введите имя пользователя",
"გთხოვთ მიუთითეთ ელ.მისამართი":"Введите электронный адрес",
"გთხოვთ მიუთითეთ პაროლი":"Введите пароль",
"ასეთი მომხმარებული უკვე რეგისტრირებულია":"Имя пользователя уже существует.",
"ასეთი ელ. ფოსტა უკვე რეგისტრირებულია":"E-mail уже существует.",
"პაროლები არ ემთხვევა ერთმანეთს":"Пароли не совпадают",
"ელ.ფოსტის მისამართი არასწორია. მაგ: example@anyemail.com":"Неверный формат электронного адреса. Пример:example@anyemail.com",
            goBackLink:"назад"
        }
    }

    const errorsLines=[
        "input-error-firstname",
        "input-error-lastname",
        "input-error-username",
        "input-error-email",
        "input-error-password",
        "input-error-password-confirm"
    ]

    document.addEventListener("DOMContentLoaded", (event) => { 
    const options = JSON.parse(localStorage.getItem("options"));
    setTimeout(()=>{ 
        if (options.firstname) {
            const firstNameField = document.getElementById('firstName');
            firstNameField.value = options.firstname;
        }

        if (options.lastname) {
            const lastNameField = document.getElementById("lastName");
            lastNameField.value = options.lastname;
        }    

        if (options.phonenumber) {
            const phoneNumberField = document.getElementById("user.attributes.phonenumber");
            phoneNumberField.value = options.phonenumber;
        }
    },100) 
});

    function handleLanguageChange () {
termsAndConditions()
        const selectedLanguage = document.getElementById("languageSelect").value;
        localStorage.setItem("localeId",selectedLanguage);
        errorsLines.map((errorLine)=>{
            const errorSpan = document.getElementById(errorLine)
            if(errorSpan){
                const text = errorSpan.innerText
                errorSpan.textContent=language[selectedLanguage][text.trim()]
            }
        })
        Object.keys(language.en).map((text)=>{
        const textSpan = document.getElementById(text)
            if(textSpan) {
                textSpan.textContent = language[selectedLanguage][text]
            }
        });
        declineTermsAndConditionsButton.value = language[selectedLanguage].decline;
        agreeToTermsAndConditionsButton.value = language[selectedLanguage].accept;
      
        createAccountButton.value = language[selectedLanguage].createAccountBtn;
        const createYourAccount = document.getElementById("kc-register-main-headline");
        switch(role) {
            case 'user':
            createYourAccount.textContent = createAccountStrings[selectedLanguage].USER_HEADLINE;
            break;
            case 'patient':
            createYourAccount.textContent = createAccountStrings[selectedLanguage].PATIENT_HEADLINE;
            break;
            case 'organization':
            createYourAccount.textContent = createAccountStrings[selectedLanguage].ORGANIZATION_HEADLINE;
            break;
            case 'doctor':
            createYourAccount.textContent = createAccountStrings[selectedLanguage].DOCTOR_HEADLINE;
            break;
        }
    }
    let locale = localStorage.getItem("localeId") ? localStorage.getItem("localeId")==="en-us" ? "en":localStorage.getItem("localeId"):"en"
    if(locale) {
        document.getElementById("languageSelect").value = locale;
        handleLanguageChange();
    }
    </script>
    </#if>

</@layout.registrationLayout>

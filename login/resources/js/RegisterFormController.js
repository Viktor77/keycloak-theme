window.addEventListener("DOMContentLoaded", handleDocumentLoaded);

class Role {
  static PATIENT = "patient";
  static DOCTOR = "doctor";
  static ORGANIZATION = "organization";
  // static USER = "user";
}

class Field {
  constructor(fieldId, inputId, labelId) {
    this.isHidden = false;
    this.field = fieldId && document.getElementById(fieldId);
    this._input = inputId && document.getElementById(inputId);
    this._label = inputId && document.getElementById(labelId);
  }

  show() {
    if (this.isHidden) {
      if (this.field !== null) {
        this.field.classList.remove("hidden-field");
      }
      this.isHidden = false;
    }
  }

  hide() {
    if (!this.isHidden) {
      if (this.field !== null) {
        this.field.classList.add("hidden-field");
      }
      this.isHidden = true;
    }
  }

  set label(value) {
    if (this._label !== null) {
      this._label.innerHTML = value;
    }
  }

  set input(value) {
    if (this._input !== null) {
      this._input.value = value;
    }
  }
}

class Svg {
  static HIDE_PATH_COLOR = "transparent";

  constructor(id) {
    this.id = id;
    this.svg = document.getElementById(id);
    this.path = this.svg !== null ? this.svg.children[0] : null;
    this.pathFill = this.path !== null ? this.path.attributes.fill : null;
    this.pathColor = this.pathFill !== null ? this.pathFill.value : null;

    this.text = this.svg !== null ? this.svg.children[1] : null;
    this.textFill = this.text !== null ? this.text.attributes.fill : null;
    this.textColor = this.textFill !== null ? this.textFill.value : null;
  }

  hide() {
    if (this.pathFill !== null) {
      this.pathFill.value = Svg.HIDE_PATH_COLOR;
      this.textFill.value = this.pathColor;
    }
  }

  show() {
    if (this.pathFill !== null) {
      this.pathFill.value = this.pathColor;
      this.textFill.value = this.textColor;
    }
  }
}

class Invite {
  constructor(inviteId, referralId, referralType,locale) {
    this.inviteId = inviteId !== null ? inviteId.trim() : "";
    this.referralId = referralId !== null ? referralId.trim() : "";
    this.referralType = referralType !== null ? referralType.trim() : "";
    this.locale=locale !== null ? locale.trim() : "";
  }
}

class RegisterFormController {
  static TERMS_AND_CONDITIONS_APPROVED = "approved";
  static TERMS_AND_CONDITIONS_DENIED = "denied";

  static DEFAULT_ORGANIZATION_FIRST_NAME = "undefined";
  static DEFAULT_ORGANIZATION_LAST_NAME = "undefined";
  static DEFAULT_USER_ORGANIZATION = "undefined";

  static languages = {
    en: {
      PATIENT_HEADLINE: "Create patient account",
      DOCTOR_HEADLINE: "Create doctor account",
      ORGANIZATION_HEADLINE: "Create organization account",
      USER_HEADLINE: "Create your account",
    },
    ka: {
      PATIENT_HEADLINE: "პაციენტის ანგარიშის შექმნა",
      DOCTOR_HEADLINE: "ექიმის ანგარიშის შექმნა",
      ORGANIZATION_HEADLINE: "ორგანიზაციის ანგარიშის შექმნა",
      USER_HEADLINE: "ანგარიშის შექმნა",
    },
    ru: {
      PATIENT_HEADLINE: "Создать аккаунт пациента",
      DOCTOR_HEADLINE: "Создать аккаунт врача",
      ORGANIZATION_HEADLINE: "Создать аккаунт организации",
      USER_HEADLINE: "Создать аккаунт",
    },
  };

  static PATIENT_HEADLINE = "Create patient account";
  static DOCTOR_HEADLINE = "Create doctor account";
  static ORGANIZATION_HEADLINE = "Create organization account";
  static USER_HEADLINE = "Create your account";

  constructor(role, invite) {
    this.role = role !== null ? role : Role.PATIENT;
    this.invite = invite
    this.getTags();
    this.setLocale();
    // this.adjustTermsAndConditions();
    this.handleRoleSelection();
    this.handleInvite();
  }

  getTags() {
    this.firstName = new Field("kc-register-first-name-field", "firstName");
    this.lastName = new Field("kc-register-last-name-field", "lastName");
    this.organization = new Field(
      "kc-register-organization-field",
      "kc-register-organization-input"
    );
    this.inviteIdInput = document.getElementById("user.attributes.inviteid");
    this.referralIdInput = document.getElementById("user.attributes.referralid");
    this.referralTypeInput = document.getElementById("user.attributes.referraltype");

    this.roleInput = document.getElementById("user.attributes.role");
    this.mainHeadline = document.getElementById("kc-register-main-headline");
    this.patientSvg = new Svg("kc-register-patient-svg");
    this.doctorSvg = new Svg("kc-register-doctor-svg");
    this.organizationSvg = new Svg("kc-register-organization-svg");
    this.userSvg = new Svg("kc-register-user-svg");
  }

  setLocale(){
    this.localeInput = document.getElementById("user.attributes.locale");
    if (this.localeInput !== null) {
      this.localeInput.value = this.invite.locale;
    }
  }

  handleInvite() {
    if (this.inviteIdInput !== null) {
      this.inviteIdInput.value = this.invite.inviteId;
    }
    if (this.referralIdInput !== null) {
      this.referralIdInput.value = this.invite.referralId;
    }
    if (this.referralTypeInput !== null) {
      this.referralTypeInput.value = this.invite.referralType;
    }

  }

  // adjustTermsAndConditions() {
  //   this.termsAndConditionsCheckBox = document.getElementById(
  //     "kc-register-terms-conditions-input"
  //   );
  //   this.termsAndConditionsInput = document.getElementById(
  //     "user.attributes.termsconditions"
  //   );
  //   if (this.termsAndConditionsCheckBox !== null) {
  //     this.termsAndConditionsCheckBox.addEventListener(
  //       "change",
  //       this.handleTermsAndConditions.bind(this)
  //     );
  //     this.handleTermsAndConditions();
  //   }
  // }

  // handleTermsAndConditions() {
  //   this.termsAndConditionsInput.value = this.termsAndConditionsCheckBox.checked
  //     ? RegisterFormController.TERMS_AND_CONDITIONS_APPROVED
  //     : RegisterFormController.TERMS_AND_CONDITIONS_DENIED;
  // }

  static selectedLanguage = localStorage.getItem("localeId")
    ? localStorage.getItem("localeId") === "en-us"
      ? "en"
      : localStorage.getItem("localeId")
    : "en";

  handleRoleSelection() {
    switch (this.role) {
      case Role.PATIENT:
        this.show([this.patientSvg, this.firstName, this.lastName]);
        this.hide([
          this.doctorSvg,
          this.organizationSvg,
          this.userSvg,
          this.organization,
        ]);
        this.mainHeadline.innerHTML =
          RegisterFormController.languages[
            RegisterFormController.selectedLanguage
          ].PATIENT_HEADLINE;
        localStorage.setItem("role", Role.PATIENT);
        break;
      case Role.DOCTOR:
        this.show([this.doctorSvg, this.firstName, this.lastName]);
        this.hide([
          this.patientSvg,
          this.organizationSvg,
          this.userSvg,
          this.organization,
        ]);
        this.mainHeadline.innerHTML =
          RegisterFormController.languages[
            RegisterFormController.selectedLanguage
          ].DOCTOR_HEADLINE;
        localStorage.setItem("role", Role.DOCTOR);
        break;
      case Role.ORGANIZATION:
        this.show([this.organizationSvg, this.organization]);
        this.hide([
          this.patientSvg,
          this.doctorSvg,
          this.userSvg,
          this.firstName,
          this.lastName,
        ]);
        this.mainHeadline.innerHTML =
          RegisterFormController.languages[
            RegisterFormController.selectedLanguage
          ].ORGANIZATION_HEADLINE;
        localStorage.setItem("role", Role.ORGANIZATION);
        break;
      // case Role.USER:
      //   this.show([this.userSvg, this.firstName, this.lastName]);
      //   this.hide([
      //     this.patientSvg,
      //     this.doctorSvg,
      //     this.organizationSvg,
      //     this.organization,
      //   ]);
      //   this.mainHeadline.innerHTML = RegisterFormController.languages[RegisterFormController.selectedLanguage].USER_HEADLINE;
      //   localStorage.setItem("role",Role.USER)
      //   break;
      default:
        console.log("Invalid role");
        return;
    }
    this.resolveNames();
    this.roleInput.value = this.role;
    console.log(`Current role is ${this.role}`);
  }

  resolveNames() {
    if (this.role !== Role.ORGANIZATION) {
      this.firstName.input = "";
      this.lastName.input = "";
      this.organization.input =
        RegisterFormController.DEFAULT_USER_ORGANIZATION;
    } else {
      this.firstName.input =
        RegisterFormController.DEFAULT_ORGANIZATION_FIRST_NAME;
      this.lastName.input =
        RegisterFormController.DEFAULT_ORGANIZATION_LAST_NAME;
      this.organization.input = "";
    }
  }

  show(items) {
    items.forEach((item) => item.show());
  }

  hide(items) {
    items.forEach((item) => item.hide());
  }
}

let registerFormController;

function handleDocumentLoaded() {
  handleRegistrationForm();
}

function handleRegistrationForm() {
  const registerForm = document.getElementById("kc-register-form");
  if (registerForm !== null) {
    console.log("Registration Form is loaded");
    const inviteId = localStorage.getItem("userRegistrationInviteId");
    const referralId = localStorage.getItem("userRegistrationReferralId");
    const referralType = localStorage.getItem("userRegistrationReferralType");
    const code = localStorage.getItem("localeId")
    let locale;
    switch (code) {
      case 'ru':
        locale = '1049';
        break;
      case 'ka':
        locale = '1079';
        break;
      default:
        locale = '1033';
    }
    const invite = new Invite(inviteId, referralId, referralType,locale);
    const role = localStorage.getItem("userRegistrationRole");
    const resolvedRole = resolveRole(role);
    console.log(`Registration account for new "${resolvedRole}"; invite id "${invite.inviteId}"; referral type "${invite.referralType}" id "${invite.referralId}"`);
    registerFormController = new RegisterFormController(resolvedRole, invite);
    // localStorage.removeItem("userRegistrationRole");
  }
}

function resolveRole(role) {
  let resolvedRole = Role.PATIENT;
  if (role !== null) {
    role = role.trim().toLowerCase();
    switch (role) {
      case Role.PATIENT:
      case Role.DOCTOR:
      case Role.ORGANIZATION:
        resolvedRole = role;
        break;
      default:
        resolvedRole = Role.PATIENT;
    }
  }
  return resolvedRole;
}
